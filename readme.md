1. Опишіть своїми словами що таке Document Object Model (DOM)
Це браузерна модель , завдяки якій ми можемо отримати будь-які елементи сторінки як об'єкти

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML - повертає текст з HTML з уразуванням тегів , а innerText повертає тільки текст 

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
 Найкращий спосіб - querySelector(), бо він є універсальним .  Також є такі способи як getElementByTagName() , getElementsByClassName.

4. Яка різниця між nodeList та HTMLCollection?
Тим , що nodeList видає нам будь-якы вузли даних, а також елементи HTML , HTMLCollection видає нам лише елементи HTML . nodeList та HTMLCollection не мають властивостей масивів , але HTMLCollection має свої ,скажем так , методи , такі як namedItem()- та item(). NodeList має такі методи отриання колекцій : getElementByTagName , getElementsByClassName , querySelector , а HTMLCollection має такі методи отриання колекцій : children , childrenNodes

